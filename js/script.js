const images = document.querySelectorAll('.image-to-show');

const seconds = document.querySelector('.seconds');
const miliseconds = document.querySelector('.miliseconds');

let i = 0;

let secondTimerId;
let timerId;
let isStoped = false;

getTimer();

function getTimer() {
    getInterval();

    timerId = setTimeout(function showImg() {
        images.forEach(img => img.classList.remove('active', 'show-in', 'show-out'));
        if (i < images.length - 1) {
            i++;
            images[i].classList.add('active', 'show-in', 'show-out');
        } else {
            i = 0;
            images[i].classList.add('active', 'show-in', 'show-out');
        }
    
        getInterval();

        timerId = setTimeout(showImg, 3000);
    }, 3000);
}

function getInterval() {
    const prevDate = Date.now();
    clearTimeout(secondTimerId);

    secondTimerId = setTimeout(function setTime() {
        seconds.innerText = '0' + Math.abs(57 - (new Date(prevDate - Date.now()).getSeconds())) + ' s';
        miliseconds.innerText = new Date(prevDate - Date.now()).getMilliseconds() + ' ms';
        secondTimerId = setTimeout(setTime, 1);
    }, 1);
}

const stopBtn = document.createElement('button');
const resumeBtn = document.createElement('button');

stopBtn.innerText = 'Stop';

resumeBtn.innerText = 'Resume';
resumeBtn.classList.add('resume');

btnholder.append(stopBtn, resumeBtn);

stopBtn.addEventListener('click' , () => {
    images.forEach(img => img.classList.remove('show-in', 'show-out'));
    clearTimeout(timerId); 
    clearTimeout(secondTimerId);
    isStoped = true;
});

resumeBtn.addEventListener('click' , () => {
    if (isStoped) {
        getTimer();
        getInterval();
    }

    images.forEach(img => img.classList.add('show-out'));
    isStoped = false;
});